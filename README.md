# t1
This is solution for the set of problems submitted as test 

**Notes**

* Solution is done in Java 8+ - my Scala is a book-level language. If you insist, I can do the same in 2-3 days.  
* I used IntelliJ as IDE - for convenience configuration files are included (which I never do in normal situation)
* Build is done with Gradle and self-contained. E.g. to run code you need just use Gradle - commands are below.
* Instead of stand-alone programs to run examples I use tests killing two birds with one stone: test and demo. Commands should be executed from ```t1``` directory

***Example 1***
This looks very simple unless I missed something. Requirements are opened for interpretation - this is the reason you will find two methods ```public static Collection<CabinPrice> getBestCabinPrices(Collection<CabinPrice> prices, int count)``` and ```public static Collection<CabinPrice> getBestCabinPricesV2(Collection<CabinPrice> prices)``` based on two different interpretation of the requirements.  
To run the test/demo use the following command  
```
	./gradlew :cleanTest :test --tests com.t1.e1.CabinPriceTests
```

***Example 2***
Whether intentional or not, example contains error in the output, considering requirement to produce ***all*** records. Example given in the document does not take into account that we need to do full n/k combinatorial search.   
Correct output looks as follows:    
```
	============================ P1 ============================
	P1 ---> P2  
	P1 ---> P4  
	P1 ---> P5  
	P1 ---> P2 P4  
	P1 ---> P2 P5  
	P1 ---> P4 P5  
	P1 ---> P2 P4 P5  
	============================ P3 ============================
	P3 ---> P2  
	P3 ---> P4  
	P3 ---> P5  
	P3 ---> P2 P4  
	P3 ---> P2 P5  
	P3 ---> P4 P5  
	P3 ---> P2 P4 P5
```
To run the demo/test use the following command: 
```
	./gradlew :cleanTest :test --tests com.t1.e1.PromotionTests
```