package com.t1.e1;

import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ToRemove
{
    /**
     * have not done combinatorics for a while :)
     */
    @Test
    public void testPerm()
    {
        String[] data = {"1", "2", "C", "4", "5", "F"};
        List<String[]> collector = new ArrayList<>();
        for (int i = 1; i<7; i++) {
            collectPermutations(data, i, 0, new String[i], collector);
        }
        System.out.println(" ============== collection ==============");
        for (String[] i: collector) {
            System.out.println(Arrays.toString(i));
        }
    }

    static void collectPermutations(String[] arr, int len, int startPosition, String[] result, List<String[]> collector)
    {
        if (len == 0) {
            System.out.println(Arrays.toString(result));
            collector.add(result.clone());
            return;
        }
        for (int i = startPosition; i <= arr.length - len; i++) {
            result[result.length - len] = arr[i];
            collectPermutations(arr, len - 1, i + 1, result, collector);
        }
    }
}
