package com.t1.e1;

import org.junit.Test;
import static org.junit.Assert.*;

import java.util.*;

public class CabinPriceTests
{
    @Test
    public void testGetBestCabinPrices()
    {
        Map<String, Rate> rates = new HashMap<>();
        rates.put("M1", new Rate("M1", "Military"));
        rates.put("M2", new Rate("M2", "Military"));
        rates.put("S1", new Rate("S1", "Senior"));
        rates.put("S2", new Rate("S2", "Senior"));

        List<CabinPrice> prices = new ArrayList<>();
        prices.add(new CabinPrice("CA", rates.get("M1"), 20000));
        prices.add(new CabinPrice("CA", rates.get("M2"), 25000));
        prices.add(new CabinPrice("CA", rates.get("S1"), 22500));
        prices.add(new CabinPrice("CA", rates.get("S2"), 26000));

        prices.add(new CabinPrice("CB", rates.get("M1"), 23000));
        prices.add(new CabinPrice("CB", rates.get("M2"), 26000));
        prices.add(new CabinPrice("CB", rates.get("S1"), 24500));
        prices.add(new CabinPrice("CB", rates.get("S2"), 27000));

        // print all values
        System.out.println("=================== All Values =================== ");
        Collection<CabinPrice> res = CabinPrice.getBestCabinPrices(prices, -1);
        res.stream().forEach( it -> System.out.println(it.toString()));
        assertEquals("Incorrect number of the records", 8, res.size());
        assertEquals("Incorrect Sorting",prices.get(0), res.iterator().next());
        // print 4 values
        System.out.println("=================== 4 Values =================== ");
        res = CabinPrice.getBestCabinPrices(prices, 4);
        res.stream().forEach( it -> System.out.println(it.toString()));
        assertEquals("Incorrect number of the limited records", 4 , res.size());
    }

    @Test
    public void testGetBestCabinPricesV2()
    {
        Map<String, Rate> rates = new HashMap<>();
        rates.put("M1", new Rate("M1", "Military"));
        rates.put("M2", new Rate("M2", "Military"));
        rates.put("S1", new Rate("S1", "Senior"));
        rates.put("S2", new Rate("S2", "Senior"));

        List<CabinPrice> prices = new ArrayList<>();
        prices.add(new CabinPrice("CA", rates.get("M1"), 20000));
        prices.add(new CabinPrice("CA", rates.get("M2"), 25000));
        prices.add(new CabinPrice("CA", rates.get("S1"), 22500));
        prices.add(new CabinPrice("CA", rates.get("S2"), 26000));

        prices.add(new CabinPrice("CB", rates.get("M1"), 23000));
        prices.add(new CabinPrice("CB", rates.get("M2"), 26000));
        prices.add(new CabinPrice("CB", rates.get("S1"), 24500));
        prices.add(new CabinPrice("CB", rates.get("S2"), 27000));

        // print all values
        System.out.println("=================== All Unique Values =================== ");
        Collection<CabinPrice> res = CabinPrice.getBestCabinPricesV2(prices);
        res.stream().forEach(it -> System.out.println(it.toString()));
//        assertEquals("Incorrect number of the records", 8, res.size());
//        assertEquals("Incorrect Sorting", prices.get(0), res.iterator().next());
    }
}
