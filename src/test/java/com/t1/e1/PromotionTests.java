package com.t1.e1;

import org.junit.Test;
import static org.junit.Assert.*;
import java.util.*;
import java.util.stream.Stream;

public class PromotionTests
{
    @Test
    public void testPermutations()
    {
        // prepare data
        Promotion P1 = new Promotion("P1");
        Promotion P2 = new Promotion("P2");
        Promotion P3 = new Promotion("P3");
        Promotion P4 = new Promotion("P4");
        Promotion P5 = new Promotion("P5");
        P1.setIncompatiblePromotions(P3);
        P2.setIncompatiblePromotions(P4, P5);
        P3.setIncompatiblePromotions(P1);
        P4.setIncompatiblePromotions(P2);
        P5.setIncompatiblePromotions(P2);
        List<Promotion> data = new ArrayList<>();
        Collections.addAll(data, P1, P2, P3, P4, P5);

        // test for P1
        System.out.println("============================ P1 ============================");
        List<List<Promotion>> res = Promotion.buildPromotionsCollection(P1, data);
        for (List<Promotion> p: res) {
            System.out.print(p.get(0).getPromotion() + " ---> ");
            for (int i = 1; i<p.size(); i++) {
                System.out.print(p.get(i).getPromotion() + " ");
            }
            System.out.println(" ");
        }
        assertEquals("Incorrect number of records", 7, res.size());
        assertFalse("Fails to exclude P3 record", res.stream().map(it -> it.contains(P3)).reduce(Boolean::logicalOr).get());


        // test for P3
        System.out.println("============================ P3 ============================");
        res = Promotion.buildPromotionsCollection(P3, data);
        for (List<Promotion> p: res) {
            System.out.print(p.get(0).getPromotion() + " ---> ");
            for (int i = 1; i<p.size(); i++) {
                System.out.print(p.get(i).getPromotion() + " ");
            }
            System.out.println(" ");
        }
        assertFalse("Fails to exclude P1 record", res.stream().map(it -> it.contains(P1)).reduce(Boolean::logicalOr).get());
    }
}
