package com.t1.e1;

import java.util.*;
import java.util.stream.Collectors;

/**
 * this class represents the promotion (as name only) and the list of the other promotions incompatible with it
 */
public class Promotion
{
    /**
     * promotion
     */
    private String promotion;

    /**
     * list of the promotions incompatible with current one
     */
    private List<Promotion> incompatiblePromotions;

    /**
     * convenience constructor
     * Note: will be really hard to use with incompatible promotions due to the circular dependencies between promotions :)
     * @param promotion
     * @param incompatibles
     */
    public Promotion(String promotion, Promotion... incompatibles)
    {
        this.promotion = promotion;
        incompatiblePromotions = new ArrayList<>();
        Collections.addAll(incompatiblePromotions, incompatibles);
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Promotion)) {
            return false;
        }
        Promotion promotion1 = (Promotion) o;
        return promotion.equals(promotion1.promotion) &&
                incompatiblePromotions.equals(promotion1.incompatiblePromotions);
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(promotion, incompatiblePromotions);
    }

    /**
     * filters out the list of the prohibited promotions from supplied list
     * @param allPromotions full (to be filtered) list of the promotions
     * @return
     */
    public Collection<Promotion> filterAllowed(Collection<Promotion> allPromotions)
    {
        return allPromotions.stream().filter(it -> !it.equals(this) && !incompatiblePromotions.contains(it)).collect(Collectors.toList());
    }

    /**
     * this method returns ALL combinations of the promotions (from the list) compatible with given one (returned as first element of each list)
     * @param base
     * @param allPromotions
     * @return
     */
    public static List<List<Promotion>> buildPromotionsCollection(Promotion base, Collection<Promotion> allPromotions)
    {
        List<List<Promotion>> result = new ArrayList<>();
        // get allowed promotions
        Collection<Promotion> allowed = base.filterAllowed(allPromotions);
        // to avoid complex manipulation with generic methods (this is doable - just permutation code will become much more complicated) - do this through the map
        Map<String, Promotion> pMap = allowed.stream().collect(Collectors.toMap(it -> it.getPromotion(), it -> it));
        // create permutations of the associated policies
        List<String[]> permutations = T1Utils.getAllPermutations(pMap.keySet().toArray(new String[pMap.keySet().size()]));
        // now create real result
        for (String[] ss: permutations) {
            List<Promotion> _r = new ArrayList<>();
            _r.add(base);
            Arrays.asList(ss).stream().forEach(it -> _r.add(pMap.get(it)));
            result.add(_r);
        }
        return result;
    }


    public String getPromotion()
    {
        return promotion;
    }

    public void setPromotion(String promotion)
    {
        this.promotion = promotion;
    }

    public List<Promotion> getIncompatiblePromotions()
    {
        return incompatiblePromotions;
    }

    public void setIncompatiblePromotions(List<Promotion> incompatiblePromotions)
    {
        this.incompatiblePromotions = incompatiblePromotions;
    }
    public void setIncompatiblePromotions(Promotion... promotions)
    {
        this.incompatiblePromotions = new ArrayList<>();
        Collections.addAll(this.incompatiblePromotions, promotions);
    }
}
