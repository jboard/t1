package com.t1.e1;

import java.util.*;
import java.util.stream.Collectors;

/**
 * defines price of the cabin for given rate
 */
public class CabinPrice
{
    /**
     * the type of the cabin
     */
    private String caninType;
    /**
     * rate for the given group
     */
    private Rate rate;
    /**
     * price - define as integer (in cents)
     */
    private Integer price;

    public CabinPrice(String caninType, Rate rate, Integer price)
    {
        this.caninType = caninType;
        this.rate = rate;
        this.price = price;
    }

    public String getCaninType()
    {
        return caninType;
    }

    public void setCaninType(String caninType)
    {
        this.caninType = caninType;
    }

    public Rate getRate()
    {
        return rate;
    }

    public void setRate(Rate rate)
    {
        this.rate = rate;
    }

    public Integer getPrice()
    {
        return price;
    }

    public void setPrice(Integer price)
    {
        this.price = price;
    }

    @Override
    public String toString()
    {
        return "CabinPrice{" +
                "caninType='" + caninType + '\'' +
                ", rate=" + rate.toString() +
                ", price=" + price +
                '}';
    }

    @Override
    public boolean equals(Object o)
    {
        if (this == o) {
            return true;
        }
        if (!(o instanceof CabinPrice)) {
            return false;
        }
        CabinPrice that = (CabinPrice) o;
        return getCaninType().equals(that.getCaninType()) &&
                getRate().equals(that.getRate()) &&
                getPrice().equals(that.getPrice());
    }

    @Override
    public int hashCode()
    {
        return Objects.hash(getCaninType(), getRate(), getPrice());
    }

    /**
     * get best cabin prices
     * Note: this is not a REAL solution since it relies on the in-memory operation. Proper solution should be based
     * on DB operation(s)
     */
    public static Collection<CabinPrice> getBestCabinPrices(Collection<CabinPrice> prices, int count)
    {
        if (prices == null || prices.size() == 0) {
            return prices;
        }
        if (count < 1) {
            return prices.stream().sorted(Comparator.comparingInt(CabinPrice::getPrice)).collect(Collectors.toList());
        } else {
            return prices.stream().sorted(Comparator.comparingInt(CabinPrice::getPrice)).limit(count).collect(Collectors.toList());
        }
    }

    /**
     * get best cabin prices per cabin/group - this is just different interpretation of the requirements.
     * Note: this is not a REAL solution since it relies on the in-memory operation. Proper solution should be based
     * on DB operation(s)
     */
    public static Collection<CabinPrice> getBestCabinPricesV2(Collection<CabinPrice> prices)
    {
        if (prices == null || prices.size() == 0) {
            return prices;
        }
        Map<String, CabinPrice> _tMap = new HashMap<>();
        prices.stream().sorted(Comparator.comparingInt(CabinPrice::getPrice).reversed()).forEach(it -> _tMap.put(it.getCaninType() + it.getRate().getRateGroup(), it));
        return _tMap.values().stream().sorted(Comparator.comparingInt(CabinPrice::getPrice)).collect(Collectors.toList());
        // can also be done in one step - just too long :)
        //return prices.stream().sorted(Comparator.comparingInt(CabinPrice::getPrice).reversed()).collect(Collectors.toMap(it -> it.getCaninType() + it.getRate().getRateGroup(), it -> it, (oldValue, newValue) -> newValue)).values().stream().sorted(Comparator.comparingInt(CabinPrice::getPrice)).collect(Collectors.toList());
    }
}
