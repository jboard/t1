package com.t1.e1;

/**
 * define mnemonics for the rate
 */
public class Rate
{
    /**
     * rate key
     */
    private String rateCode;
    /**
     * rate name
     */
    private String rateGroup;

    public Rate(String rateCode, String rateGroup)
    {
        this.rateCode = rateCode;
        this.rateGroup = rateGroup;
    }

    public String getRateCode()
    {
        return rateCode;
    }

    public void setRateCode(String rateCode)
    {
        this.rateCode = rateCode;
    }

    public String getRateGroup()
    {
        return rateGroup;
    }

    public void setRateGroup(String rateGroup)
    {
        this.rateGroup = rateGroup;
    }

    @Override
    public String toString()
    {
        return "Rate{" +
                "rateCode='" + rateCode + '\'' +
                ", rateGroup='" + rateGroup + '\'' +
                '}';
    }
}

