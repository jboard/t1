package com.t1.e1;

import java.util.ArrayList;
import java.util.List;

/**
 * set of the utils used in the code
 */
public class T1Utils
{

    /**
     * recursively calculates permutations for the given collection of strings - does not enforce uniqueness
     * this is code is faster and consumes less memory (compare to the list based implementation) since it operates of the immutable data (array)
     * and does not allocate any additional memory
     * @param arr data
     * @param len desired length of the permutations
     * @param startPosition starting position
     * @param result recursive data collector
     * @param collector overall result collector
     */
    private static void collectPermutations(String[] arr, int len, int startPosition, String[] result, List<String[]> collector)
    {
        if (len == 0) {
            collector.add(result.clone());
            return;
        }
        for (int i = startPosition; i <= arr.length - len; i++) {
            result[result.length - len] = arr[i];
            collectPermutations(arr, len - 1, i + 1, result, collector);
        }
    }

    /**
     * get list of all permutations
     * @param data list of the strings we would like to do full permutations on
     */
    public static List<String[]> getAllPermutations(String[] data)
    {
        ArrayList<String[]> res = new ArrayList<>();
        if (data == null && data.length == 0) {
            return res;
        }

        for (int i = 1; i<=data.length; i++) {
            collectPermutations(data, i, 0, new String[i], res);
        }

        return res;
    }

}
